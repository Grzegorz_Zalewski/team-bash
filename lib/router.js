Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notFound',
    waitOn: function () {
        return [Meteor.subscribe('notifications')]
    }
});
var requireLogin = function () {
    if (!Meteor.user()) {
        if (Meteor.loggingIn()) {
            this.render(this.loadingTemplate);
        } else {
            this.render('accessDenied');
        }
    } else {
        this.next();
    }
};
PostsListController = RouteController.extend({
    template: 'postsList',
    increment: 6,
    postsLimit: function () {
        return parseInt(this.params.postsLimit) || this.increment;
    },
    findOptions: function () {
        return {sort: this.sort, limit: this.postsLimit()};
    },
    subscriptions: function () {
        this.postsSub = Meteor.subscribe('posts', this.findOptions());
    },
    posts: function () {
        return Posts.find({}, this.findOptions());
    },
    postsByTag: function (tagid) {
        return Posts.find({}, this.findOptions());
    },
    data: function () {
        var self = this;
        return {
            posts: self.posts(),
            ready: self.postsSub.ready,
            nextPath: function () {
                if (self.posts().count() === self.postsLimit())
                    return self.nextPath();
            }
        };
    }
});

NewPostsController = PostsListController.extend({
    sort: {submitted: -1, _id: -1},
    nextPath: function () {
        return Router.routes.newPosts.path({postsLimit: this.postsLimit() + this.increment})
    }
});

BestPostsController = PostsListController.extend({
    sort: {votes: -1, submitted: -1, _id: -1},
    nextPath: function () {
        return Router.routes.bestPosts.path({postsLimit: this.postsLimit() + this.increment})
    }
});
SearchByTagController = PostsListController.extend({
    sort: {submitted: -1, _id: -1},
    nextPath: function () {
        return Router.routes.searchByTag.path({tag: this.params.tag, postsLimit: this.postsLimit() + this.increment})
    },
    posts: function () {
        return Posts.find({"tags.name": this.params.tag}, this.findOptions());
    },
    subscriptions: function () {
        this.postsSub = Meteor.subscribe('searchByTag', this.params.tag, this.findOptions());
    }
});
PostsByAuthorController = PostsListController.extend({
    sort: {submitted: -1, _id: -1},
    nextPath: function () {
        return Router.routes.postsByAuthor.path({
            username: this.params.username,
            name: this.params.name,
            postsLimit: this.postsLimit() + this.increment
        })
    },
    subscriptions: function () {
        this.postsSub = Meteor.subscribe('postsByAuthor', this.params.username, this.findOptions());
    },
    posts: function () {
        return Posts.find({"author.username": this.params.username}, this.findOptions());
    }
});

Router.route('/', {
    name: 'home',
    controller: NewPostsController
});
Router.route('/login', {
    name: 'login',
    controller: NewPostsController
});
// reload loop protection
Router.route('/search/', {controller: NewPostsController});
Router.route('/search/tag', {controller: NewPostsController});
Router.route('/author/', {controller: NewPostsController});
Router.route('/posts/', {controller: NewPostsController});

Router.route('/new/:postsLimit?', {name: 'newPosts'});
Router.route('/best/:postsLimit?', {name: 'bestPosts'});
Router.route('/search/tag/:tag/:postsLimit?', {name: 'searchByTag'});
Router.route('/author/:username/:name?/:postsLimit?', {name: 'postsByAuthor'});
Router.route('/search/:text/:postsLimit?', {
    name: 'search', template: 'searchList', waitOn: function () {
        var term = this.params.text;
        var postsLimit = this.params.postsLimit;
        return Meteor.subscribe('search', term, postsLimit);
    }
});


Router.route('/posts/:_id', {
    name: 'postPage',
    waitOn: function () {
        return [
            Meteor.subscribe('singlePost', this.params._id),
            Meteor.subscribe('comments', this.params._id)
        ];
    },
    data: function () {
        return Posts.findOne(this.params._id);
    }
});


Router.route('/posts/:_id/edit', {
    name: 'postEdit',
    waitOn: function () {
        return [
            Meteor.subscribe('singlePost', this.params._id),
            Meteor.subscribe('allTags')
        ];
    },
    data: function () {
        return Posts.findOne(this.params._id);
    }
});


Router.route('/submit', {
    name: 'postSubmit',
    waitOn: function () {
        return Meteor.subscribe('allTags');
    }
});


Router.onBeforeAction('dataNotFound', {only: 'postPage'});
Router.onBeforeAction(requireLogin, {only: 'postSubmit'});
Router.onBeforeAction(requireLogin, {only: 'login'});
