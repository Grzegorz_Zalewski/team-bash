BLOCK_INDEX = false;
Posts = new Mongo.Collection('posts');
Posts.after.remove(function (userId, post) {
    Meteor.call("EsDelete", post._id, function (error, result) {
        if (error) {
            throwError(error.reason);
        }
    });
});
Posts.after.update(function (userId, post) {
    if (this.previous.commentsCount === post.commentsCount)
        Meteor.call("EsUpdate", post._id, post, function (error, result) {
            if (error) {
                throwError(error.reason);
            }
        });
});
Posts.after.insert(function (userId, post) {
    if (!BLOCK_INDEX)
        Meteor.call("EsAdd", post._id, post, function (error, result) {
            if (error) {
                throwError(error.reason);
            }
        });
});
Posts.allow({
    update: function (userId, post) {
        return ownsDocument(userId, post);
    },
    remove: function (userId, post) {
        return ownsDocument(userId, post);
    }
});

Posts.deny({
    update: function (userId, post, fieldNames) {
        // may only edit the following two fields:
        return (_.without(fieldNames, 'content', 'tags').length > 0);
    }
});

Posts.deny({
    update: function (userId, post, fieldNames, modifier) {
        var errors = validatePost(modifier.$set);
        return errors.tags || errors.content;
    }
});


validatePost = function (post) {
    var errors = {};
    if (!post.content)
        errors.content = "Hola, nie wpisałeś powiedzonka";
    return errors;
};

Meteor.methods({
    postInsert: function (postAttributes) {
        check(this.userId, String);
        check(postAttributes, {
            tags: Array,
            content: String
        });

        var errors = validatePost(postAttributes);
        if (errors.content)
            throwError('Musisz coś wpisac w treści powiedzonka');

        var postWithSameContent = Posts.findOne({content: postAttributes.content});
        if (postWithSameContent) {
            return {
                postExists: true,
                _id: postWithSameContent._id
            }
        }
        var fulltags = prepareTags(postAttributes.tags);
        postAttributes.tags = fullToShortTags(fulltags);

        var user = Meteor.user();
        var post = _.extend(postAttributes, {
            userId: user._id,
            author: _.pick(user, "username", "profile"),
            submitted: new Date(),
            commentsCount: 0,
            upvoters: [],
            votes: 0
        });

        var postId = Posts.insert(post);
        updateTags(fulltags, postId);
        return {
            _id: postId
        };
    },
    postUpdate: function (oldPost, newPost) {
        check(oldPost, Match.Any);
        check(newPost, {
            tags: Array,
            content: String
        });
        var fulltags = prepareTags(newPost.tags);
        var submitPost = newPost;
        submitPost.tags = fullToShortTags(fulltags);
        Posts.update(oldPost._id, {$set: submitPost});
        updatePostTags(oldPost, newPost)
    },
    removePost: function (postId) {
        check(postId, String);
        Posts.remove(postId);
        removeRelatedTags(postId);
    },
    upvote: function (postId) {
        check(this.userId, String);
        check(postId, String);

        var affected = Posts.update({
            _id: postId,
            upvoters: {$ne: this.userId}
        }, {
            $addToSet: {upvoters: this.userId},
            $inc: {votes: 1}
        });

        if (!affected) {
            throwError("Nie masz prawa głosu");
        } else {
            var post = Posts.findOne({
                _id: postId
            });
            var user = Meteor.users.findOne({
                _id: this.userId
            });
            if (post !== undefined && user !== undefined) {
                createLikeNotification(post, user)
            }

        }
    }
});
