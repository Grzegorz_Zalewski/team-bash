Tags = new Mongo.Collection('tags');


prepareTags = function (rawtags) {
    var clearTags = [];
    if(rawtags[rawtags.length - 1]!==null)
    rawtags[rawtags.length - 1].forEach(function (tag) {
        var db = Tags.findOne({name: tag});
        if (db == null) {
            var newtag = {name: tag.toLowerCase(), posts: [], color: Helpers.random(Helpers.colors())};
            clearTags.push(Tags.findOne({_id:Tags.insert(newtag)}));
        } else {
            clearTags.push(db);
        }
    });
    return clearTags
};
shortToFullTags = function (rawtags) {
    var clearTags = [];
    rawtags.forEach(function (tag) {
        var name = tag.name||tag;
        name=name.toLowerCase();
        var db = Tags.findOne({name:name});
        if (db == null) {
            var newtag = {name:name, posts: [], color: Helpers.random(Helpers.colors())};
            clearTags.push(Tags.findOne({_id:Tags.insert(newtag)}));
        } else {
            clearTags.push(db);
        }
    });
    return clearTags
};
updatePostTags= function(oldPost,newPost){
    var newIds = [];
    var oldIds = [];
    oldPost.tags.forEach(function(obj){
        oldIds.push(obj.name);
    });
    newPost.tags.forEach(function(obj){
        newIds.push(obj.name);
    });

    var idsToRemove = _.difference(oldIds, newIds)||[];
    var idsToAdd = _.difference(newIds, oldIds)||[];

    idsToRemove.forEach(function (tag) {
        removeTag(tag, oldPost._id);
    });
    idsToAdd.forEach(function (tag) {
        updateTag(tag, oldPost._id);
    });
    Tags.remove({ "posts": { $size:0}});

};
removeRelatedTags = function(postid){
    Tags.update({"posts": postid}, {$pull: {"posts": postid}} );
    Tags.remove({ "posts": { $size:0}});
};

removeTag = function(tagname, postid){
    Tags.update({"name": tagname}, {$pull: {"posts": postid}} );
};


fullToShortTags = function (fulltags) {
    var shortTags = [];

    fulltags.forEach(function (tag) {
        shortTags.push(_.pick(tag,"name","color"));
    });
    return shortTags
};
updateTags = function (fulltags, postid) {
    fulltags.forEach(function (tag) {
        Tags.update({
            _id: tag._id,
            posts: {$ne: postid}
        }, {
            $addToSet: {posts: postid}
        });
    });
};
updateTag = function (tagname, postid) {
        Tags.update({
            name: tagname,
            posts: {$ne: postid}
        }, {
            $addToSet: {posts: postid}
        });
};
Meteor.methods({});
