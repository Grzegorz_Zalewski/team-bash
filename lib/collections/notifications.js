Notifications = new Mongo.Collection('notifications');

Notifications.allow({
    update: function (userId, doc, fieldNames) {
        return ownsDocument(userId, doc) &&
            fieldNames.length === 1 && fieldNames[0] === 'read';
    }
});
createCommentNotification = function (comment) {
    var post = Posts.findOne(comment.postId);
    if (comment.userId !== post.userId) {
        Notifications.insert({
            userId: post.userId,
            postId: post._id,
            commentId: comment._id,
            commenter: _.pick(comment.author, "username", "profile"),
            submitted: new Date(),
            actionType: 2,
            read: false
        });
    }
};
createLikeNotification = function (post, user) {
    if (user._id !== post.userId) {
        Notifications.insert({
            userId: post.userId,
            postId: post._id,
            commenter: _.pick(user, "username", "profile"),
            submitted: new Date(),
            actionType: 1,
            read: false
        });
    }
};
createAdminNotification = function (content, user) {
    Notifications.insert({
        userId: user._id,
        content: content,
        submitted: new Date(),
        actionType: 0,
        read: false
    });
};
createBrodcastNotification = function (content) {
    Meteor.users.find().forEach(function (user) {
        createAdminNotification(content, user)
    });

};
deleteNotification = function () {
    Notifications.remove({read: true});
    Notifications.remove({actionType: 0});
};

Meteor.methods({
    brodcast: function (content) {
        check(content, String);
        createBrodcastNotification(content)
    },
    notyfy: function (content, username) {
        check(content, String);
        check(username, String);
        var user = Meteor.users.findOne({username: username});
        createAdminNotification(content, user)
    },
    release: function () {
        createBrodcastNotification("Właśnie releasujemy nową wersje. Aplikacja może działać niestabilnie. Zapraszamy ponownie za 5 minut")
    },
    noRelease: function () {
        deleteNotification();
    }
});

