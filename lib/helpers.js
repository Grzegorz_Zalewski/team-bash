Helpers = {
    random : function (items) {
        return items[Math.floor(Math.random() * items.length)];
    },
    colors : function () {
        return ["pink", "grey", "orange", "olive", "green", "teal", "blue", "violet", "purple", "brown", "black"];
    },
    errorHeader : function () {
        return ["Kurcze Coś się wykasztaniło!", "No i cały misterny plan poszedł  się je...", "Coś poszło nie tak", "CiviCRM error occurred", "error z dupy", "Hiten fixed. Now not working.", "To nie ma prawa się wysypać"];
    },
    avatar: function (id, size) {
        if (id==null){
            return "http://shp.future-processing.com/_layouts/15/images/PersonPlaceholder.200x150x32.png"
        }else{
            return  "http://shp.future-processing.com/my/User%20Photos/Profile%20Pictures/"+
                id +"_"+size+"Thumb.jpg"
        }
    }
};


