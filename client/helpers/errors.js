// Local (client-only) collection
Errors = new Mongo.Collection("Errors");

throwError = function (message) {
    sAlert.error('<b>'+Helpers.random(Helpers.errorHeader())+'</b> <br> '+message, {effect: 'bouncyflip', html: true});
};