Template.registerHelper('pluralize', function (n, thing) {
    // fairly stupid pluralizer
    if (n === 1) {
        return '1 ' + thing;
    } else {
        return n + ' ' + thing + 'ów';
    }
});
Template.registerHelper('avatar', function (id, size) {
 return Helpers.avatar(id,size)
});
Template.registerHelper('test', function (obj) {
   console.log(JSON.stringify(obj, null, "\t"));
});
Template.registerHelper('arrayify',function(obj){
    result = [];
    for (var key in obj) result.push(obj[key]);
    return result;
});
Template.registerHelper('random',function(items){
        return Helpers.random(items)
});
Template.registerHelper('imageError',function(){
        return "this.onerror=null;this.src='/img/matt.jpg';";
});

Template.registerHelper('color',function(){
        return Helpers.colors()
});
Template.registerHelper('incentives',function(){
        return ["Co zasłyszałeś...", "Podziel się czymś śmiesznym...", "Kto zasłużył by tu trafić...", "Jaka jest twoja historia...", "Ty uderzać w klawisze, ja wyświetlać znaczki...", "Szkryflaj co furgo w lufcie..."];
});
Template.registerHelper('isEqual',function(a,b){
        return a===b;
});
Template.registerHelper('moment',function(time){
    return moment(time).fromNow();
});
