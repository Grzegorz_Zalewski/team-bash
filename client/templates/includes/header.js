Template.header.helpers({
    activeRouteClass: function (/* route names */) {
        var args = Array.prototype.slice.call(arguments, 0);
        args.pop();

        var active = _.any(args, function (name) {
            return Router.current() && Router.current().route.getName() === name
        });

        return active && 'active';
    }
});
var query = function ($target) {
    var query
    if(typeof $target === 'string'){
        query = $target;
    }
    else{
      query= $target.val();
    }
    if (query !== '') {
        Router.go('search', {text: query});
    } else {
        Router.go("home");
    }
};
var typing = _.debounce(function (e) {
    query($(e.target));
}, 600);
Template.header.events({
    'click .ui.black.big.launch': function (e) {
        $('.ui.sidebar')
            .sidebar('toggle');
    },
    'click .logout': function (e) {
        event.preventDefault();
        Meteor.logout();
    },
    'keyup .prompt.search.nav-search': function (e) {
        if (e.which === 13) {
            query($(e.target));
        } else {
            typing(e);
        }
    },
    'click .search.link.icon': function (e) {
        var $target = $("input.prompt.search", $(e.target).parent()).val();
        query($target);

    }

});

