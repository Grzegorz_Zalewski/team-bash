Template.notifications.helpers({
    notifications: function () {
        return Notifications.find({userId: Meteor.userId(), read: false});
    },
    notificationCount: function () {
        return Notifications.find({userId: Meteor.userId(), read: false}).count();
    },

    notify: function (message) {
        Notifications.find({actionType: 3}).forEach(function (message) {
            sAlert.info(message.content, {effect: 'bouncyflip', html: true});
        })
    }

});

Template.notificationItem.onRendered(function () {
    $(".notifications").dropdown();
  var data = this.data;
  var title , body , icon;

    switch(data.actionType) {
      case 1:
      title = "Nowy like";
      body = data.commenter.profile.displayName + " polubił(a) twoje powiedzonko";
      icon = Helpers.avatar(data.commenter.username,"M");
    break;
  case 2:
    title = "Nowy komentarz";
    body = data.commenter.profile.displayName + " skomentował(a) twoje powiedzonko";
    icon = Helpers.avatar(data.commenter.username,"M");
    break;
  case 0:
    title = "Wiadomośc od administracji";
    body = data.content;
    icon = "/img/matt.jpg";
    break;
  }
  notify.createNotification(title, {
    body: body,
    icon: icon
  });
});

Template.notificationItem.helpers({
/*    notificationPostPath: function () {

    },*/
/*    created: function () {
        return moment(this.submitted).fromNow();
    }*/
});

Template.notificationItem.events({
    'click a.notificationItem': function () {
        Notifications.update(this._id, {$set: {read: true}});
        if(this.actionType!==0){
            Router.go("postPage", {_id: this.postId});
        }
    }
});