Template.layout.onRendered(function () {
    this.find('#main')._uihooks = {
        insertElement: function (node, next) {
            $(node)
                .hide()
                .insertBefore(next)
                .fadeIn();
        },
        removeElement: function (node) {
            $(node).fadeOut(function () {
                $(this).remove();
            });
        }
    };
    // prevent submit form on enter and go back on backspace
    $(document).on("keydown", function (e) {
        if (e.which === 8 && !$(e.target).is("input, textarea")||
            e.which === 13&& $(e.target).is("form") ) {
            e.preventDefault();
        }
    });

    if(notify.isSupported && notify.permissionLevel() === notify.PERMISSION_DEFAULT){
        notify.requestPermission();
    }
});
Template.layout.events({
    'click .logout': function(event){
        event.preventDefault();
        Meteor.logout();
    }
});
Template.layout.helpers({
    activeRouteClass: function (/* route names */) {
        var args = Array.prototype.slice.call(arguments, 0);
        args.pop();

        var active = _.any(args, function (name) {
            return Router.current() && Router.current().route.getName() === name
        });

        return active && 'active';
    },
    templateGestures: {
        hammerInitOptions: {
            cssProps: { userSelect: 'all' }
        },
        'swiperight #main , dragright #main': function (event, templateInstance) {
            $('.ui.sidebar').sidebar('show');
        }
/*        'swipeleft div': function (event, templateInstance) {
            $('.ui.sidebar').sidebar('hide');
        }*/
    }
});
