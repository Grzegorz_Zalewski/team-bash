Template.commentSubmit.onCreated(function () {
    Session.set('commentSubmitErrors', {});
});

Template.commentSubmit.helpers({
    errorMessage: function (field) {
        return Session.get('commentSubmitErrors')[field];
    },
    errorClass: function (field) {
        return !!Session.get('commentSubmitErrors')[field] ? 'error' : '';
    },
    anyErrorClass: function () {
        return jQuery.isEmptyObject(Session.get('commentSubmitErrors')) ? '' : 'error';
    },
    userlogin: function () {
        return Meteor.user() != null;
    }
});

Template.commentSubmit.events({
    'submit form': function (e, template) {
        e.preventDefault();

        var $body = $(e.target).find('[name=body]');
        var comment = {
            body: $body.val(),
            postId: template.data._id
        };

        var errors = {};
        if (!comment.body) {
            errors.body = "Nie ma co dodawać popraw pole wyżej";
            return Session.set('commentSubmitErrors', errors);
        }

        Meteor.call('commentInsert', comment, function (error, commentId) {
            if (error) {
                throwError(error.reason);
            } else {
                $body.val('');
            }
        });
    }
});