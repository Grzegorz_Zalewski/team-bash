Template.postEdit.onCreated(function () {
    Session.set('postEditErrors', {});
});
Template.postEdit.rendered = function () {
    $("#tags").dropdown(
        {
            allowAdditions: true,
            showOnFocus:false
        }
    );
    this.data.tags.forEach(function(tag){
        $("#tags").dropdown("set selected",tag.name)
    });
};

Template.postEdit.helpers({
    errorMessage: function (field) {
        return Session.get('postEditErrors')[field];
    },
    errorClass: function (field) {
        return !!Session.get('postEditErrors')[field] ? 'has-error' : '';
    },
    allTags: function(){
        return Tags.find();
    }
});

Template.postEdit.events({
    'submit form': function (e) {
        e.preventDefault();

        var currentPostId = this._id;

        var postProperties = {
            content: $(e.target).find('[name=content]').val(),
            tags: $(e.target).find('[name=tags]').dropdown("get value")
        };

        var errors = validatePost(postProperties);
        if (errors.tags || errors.content)
            return Session.set('postEditErrors', errors);
        Meteor.call('postUpdate', this, postProperties, function (error, result) {
            // display the error to the user and abort
            if (error) {
                // display the error to the user
                throwError(error.reason);
            } else {
                Router.go('postPage', {_id: currentPostId});
            }
        });


    },

    'click .delete-post': function (e) {
        e.preventDefault();
        var currentPostId = this._id;
        $('.ui.basic.modal')
            .modal({
                closable: false,
                onDeny: function () {
                },
                onApprove: function () {
                    Meteor.call("removePost",currentPostId);
                    Router.go('home');
                }
            })
            .modal('show');
    }

});
