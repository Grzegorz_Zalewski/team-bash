Template.postItem.helpers({
    ownPost: function () {
        return this.userId == Meteor.userId();
    },
    domain: function () {
        var a = document.createElement('a');
        a.href = this.url;
        return a.hostname;
    },
    upvotedClass: function () {
        var userId = Meteor.userId();
        if (userId && !_.include(this.upvoters, userId)) {
            return 'disabled';
        } else {
            return 'red';
        }
    },
    created: function () {
        return moment(this.submitted).fromNow();
    }

});

Template.postItem.events({
    'click .disabled.vote': function (e) {
        e.preventDefault();
        var post = $(e.target).closest(".segment");
        post.dimmer('show');
        setTimeout(function () {
            post.dimmer('hide');
        }, 3000);
        Meteor.call('upvote', this._id);
    }
});