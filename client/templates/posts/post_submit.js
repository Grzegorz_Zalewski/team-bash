Template.postSubmit.onCreated(function () {
    Session.set('postSubmitErrors', {});

});
Template.postSubmit.rendered = function () {
    $("#tags").dropdown(
        {
            allowAdditions: true,
            showOnFocus:false
        }
    );
};

Template.postSubmit.helpers({
    errorMessage: function (field) {
        return Session.get('postSubmitErrors')[field];
    },
    errorClass: function (field) {
        return !!Session.get('postSubmitErrors')[field] ? 'error' : '';
    },
    anyErrorClass: function () {
        return jQuery.isEmptyObject(Session.get('postSubmitErrors')) ? '' : 'error';
    },
    allTags: function(){
            return Tags.find();
    }
});

Template.postSubmit.events({
    'submit form': function (e) {
        e.preventDefault();

        var post = {
            content: $(e.target).find('[name=content]').val(),
            tags: $(e.target).find('[name=tags]').dropdown("get value")
        };

        var errors = validatePost(post);
        if (errors.content)
            return Session.set('postSubmitErrors', errors);

        Meteor.call('postInsert', post, function (error, result) {
            // display the error to the user and abort
            if (error)
                return throwError(error.reason);

            // show this result but route anyway
            if (result.postExists)
                throwError('Ktoś juz to wrzucił');

            Router.go('postPage', {_id: result._id});
        });
    }
});