Meteor.startup(function () {
    sAlert.config({
        effect: 'bouncyflip',
        position: 'bottom',
        timeout: 0,
        html: false,
        onRouteClose: false,
        stack: {
            spacing: 10, // in px
            limit: 3 // when fourth alert appears all previous ones are cleared
        },
        offset: 0, // in px - will be added to first alert (bottom or top - depends of the position in config)
        // examples:
        // beep: '/beep.mp3'  // or you can pass an object:
        beep: {
            info: '/info.mp3',
            error: '/error.mp3',
            success: '/info.mp3',
            warning: '/info.mp3'
        }
    });

});
