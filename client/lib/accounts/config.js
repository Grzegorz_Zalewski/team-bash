Accounts.config({
    forbidClientAccountCreation: true
});
Meteor.loginWithPassword = function (user, password, callback){
    Meteor.loginWithLdap(user.email,password,callback)
};
LDAP.data = function () {
    return {
        tenant_id: "fp"
    };
};
Meteor.loginWithLdap = function (username, password, callback) {
    username=username.split("@",1)[0];
    var methodArguments = {username: username, pwd: password, ldap: true, data: LDAP.data()};
    Accounts.callLoginMethod({
        methodArguments: [methodArguments],
        validateResult: function (result) {
        },
        userCallback: callback
    });
};
AccountsTemplates.configure({
    // Behavior
    enablePasswordChange: false,
    forbidClientAccountCreation: true,
    sendVerificationEmail: false,
    lowercaseUsername: true,
    focusFirstInput: true,

    // Appearance
    showAddRemoveServices: false,
    showForgotPasswordLink: false,
    showLabels: true,
    showResendVerificationEmailLink: false
});
T9n.map( 'pl',{
    error:{
        accounts:{
            'Invalid credentials': 'Chyba nie do końca to przemyślałeś'}}})