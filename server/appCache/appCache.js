Meteor.AppCache.config({
    chrome: false,
    firefox: true,
    android:true,
    chromium:true,
    chromeMobileIOS:true,
    ie:true,
    mobileSafari:true,
    safari:true,
    onlineOnly: ['/public/']
});