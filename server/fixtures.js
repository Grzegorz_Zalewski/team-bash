// Fixture data
function reproduceDatabase() {
    BLOCK_INDEX = true;

    var now = new Date().getTime();
    // create user
    var borysId = Meteor.users.insert({
        profile: {displayName: 'Stary Borys', color: "grey" },username: "pborysowski"});
    var borys = Meteor.users.findOne(borysId);

    /* old db don't exist;*/
    var database = new MongoInternals.RemoteCollectionDriver("mongodb://admin:admin@vm-ts-gzalewski:27017/db");
    var Borysdatabase = new Mongo.Collection("notesproviders", {_driver: database});
    Borysdatabase.find().forEach(function (element) {
        if (!element.hashTags) {
            element.hashTags = [];
        }
        else {
            var htags = [];
            element.hashTags.forEach(function (row) {
                var tag = {};
                var id = element._id._str;
                if (Tags.find({name: row.toLowerCase()}, {limit: 1}).count() > 0) {
                    Tags.update({
                        name: row.toLowerCase()
                    }, {
                        $push: {posts: id}
                    });
                    tag = Tags.findOne({name: row.toLowerCase()})
                }
                else {

                    tag = {name: row.toLowerCase(), posts: [id], color: Helpers.random(Helpers.colors())};
                    Tags.insert(tag);
                }
                htags.push(_.pick(tag,"name","color"));
            })
        }
        Posts.insert({
            tags: htags || [],
            userId: borys._id,
            author: _.pick(borys,"username","profile"),
            content: element.text,
            submitted: element.created_at,
            actors: [],
            commentsCount: 0,
            upvoters: [], votes: element.votes
        });

    });
    BLOCK_INDEX = false;
    ES.bulk({
        body: ES.createBatch(Posts.find())
    },  ES.error);
}

if (Posts.find().count() === 0) {
     reproduceDatabase();
}





