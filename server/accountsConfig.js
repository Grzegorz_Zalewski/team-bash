/**
 * Created by grzegorz on 01.09.15.
 */
LDAP.logging = false;
LDAP.multitenantIdentifier = 'tenant_id';
// Overwrite this function to produce settings based on the incoming request
LDAP.generateSettings = function (request) {
    return {
        "serverDn": "DC=fp,DC=lan",
        "serverUrl": "ldap://fp-ad-1.fp.lan:389",
        "whiteListedFields": [ "displayName" ],
        "userDn":process.env.FP_ADMIN_LOGIN+"@future-processing.com",
        "passwordDn":process.env.FP_ADMIN_PASS

    }
};
LDAP.filter = function (email, username) {
    return '(&(' + ((email) ? 'mail' : 'mailNickname') + '=' + username + ')(objectClass=user))';
};
Accounts.onCreateUser(function (options, user) {
    user.profile = {};
    user.profile.displayName=options.profile.displayName;
    user.profile.color=Helpers.random(Helpers.colors());
    return user;
});


