Meteor.publish('posts', function (options) {
    check(options, {
        sort: Object,
        limit: Number
    });
    return Posts.find({}, options);
});

Meteor.publish('singlePost', function (id) {
    check(id, String);
    return Posts.find(id);
});


Meteor.publish('comments', function (postId) {
    check(postId, String);
    return Comments.find({postId: postId});
});
Meteor.publish('search', function (term, limit) {
    check(term, String);
    check(limit, Match.Any);
    var self = this;
    Meteor.call('search', term, limit, function (error, result) {
        if (result.result) {
            result.result.forEach(function (el) {
                self.added("search", el._id, el);
            });
                self.added("searchInfo", 1, {
                    searchTerm: term,
                    TotalHits: result.hits,
                    searchLimit: limit
                });
            self.ready();
        } else {
            self.ready();
        }
    });
});

Meteor.publish("postsByAuthor", function (authorUsername, options) {
    check(authorUsername, String);
    check(options, {
        sort: Object,
        limit: Number
    });
    if (!authorUsername) {
        return Posts.find({}, options);
    }
    return Posts.find({"author.username": authorUsername}, options)

});
Meteor.publish('searchByTag', function (tag, options) {
    check(tag, String);
    check(options, {
        sort: Object,
        limit: Number
    });
    return Posts.find({"tags.name": tag}, options);
});

Meteor.publish('notifications', function () {
    return Notifications.find({userId: this.userId, read: false});
});
Meteor.publish('allTags', function () {
    return Tags.find();
});
