String.prototype.trunc =
    function(n){
        var trimmedString = this.substr(0, n);
        trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
        return trimmedString+"...";
    };
RssFeed.publish('najnowsze', function(query) {
    var self = this;
    self.setValue('title', self.cdata('Najnowsze powiedzonka'));
    self.setValue('description', self.cdata('Najnowsze powiedzonka techupów'));
    self.setValue('link', 'http://vm-powiedzonka');
    self.setValue('lastBuildDate', new Date());
    self.setValue('pubDate', new Date());
    self.setValue('ttl', 1);
    // managingEditor, webMaster, language, docs, generator

    Posts.find({},{sort:{"submitted": -1},limit:50}).forEach(function(doc) {
        var categorys ="";
        doc.tags.forEach(function(tag){
            categorys+="<category>"+tag.name+"</category>"
        });
        self.addItem({
            title: doc.content.trunc(100),
            description: doc.content,
            category:categorys,
            author:doc.author.profile.displayName,
            image:Helpers.avatar(doc.author.username,"L"),
            language:"pl",
            link: 'http://vm-powiedzonka/posts/'+doc._id,
            pubDate: doc.submitted
        });
    });

});