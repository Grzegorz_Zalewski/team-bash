GElastic = function (index, type) {

    ElasticSearch = Meteor.npmRequire('elasticsearch');
    this.timeout = process.env.ELASTICSEARCH_TIMEOUT;
    // create the client
    var EsClient = new ElasticSearch.Client({
        host: process.env.ELASTICSEARCH_HOST,
        log: process.env.ELASTICSEARCH_LOGLEVEL
    });
    EsClient.index = index;
    EsClient.type = type;
    EsClient.error = function (error, response) {
        if (error) {
            throwError('Elastic search error ' + error.message + "<br> " + JSON.stringify(error, null, "\t"));
        }
    };
    EsClient.createBatch = function (cursor) {
        var array = [];
        cursor.forEach(function (doc) {
            array.push({index: {_index: index, _type: type, _id: doc._id}});
            array.push({
                tags: doc.tags,
                author: doc.author.profile.displayName,
                content: doc.content,
                actors: doc.actors
            });
        });
        return array;
    };
    EsClient.waitOnSearch = function (word, size) {
        var query = {
            "bool": {
                "should": [
                    {
                        "fuzzy_like_this": {
                            "fields": [
                                "post.tags",
                                "post.content",
                                "post.author"
                            ],
                            "like_text": word,
                            "max_query_terms": 12,
                            "analyzer": "polish"
                        }
                    }
                ]
            }
        };
        var body = {
            query: query,
            size: size,
            fields: [
                "_id"
            ]
        };
        var syncSearch = Async.wrap(this, 'search');
        var searchData = syncSearch({
            index: this.index,
            type: this.type,
            body: body
        });
        return searchData;
    };
    EsClient.waitOnSuggest = function (text, field) {
        var syncSuggest = Async.wrap(this, 'suggest');
        ;
        var response = syncSuggest({
            index: ES.index,
            body: {
                mysuggester: {
                    text: text,
                    term: {
                        field: field
                    }
                }
            }
        });
        return response;
    };
    return EsClient;
};
ES = GElastic("powiedzonka", "post");
Meteor.methods({
    EsAdd: function (id, document) {
        check(id, String);
        check(document, Match.Any);
        ES.create({
            index: ES.index,
            type: ES.type,
            analyzer: "polish",
            id: id,
            body: {
                tags: document.tags,
                author: document.author.profile.displayName,
                content: document.content,
                actors: document.actors
            }
        }, ES.error);
    },
    EsUpdate: function (id, document) {
        check(id, String);
        check(document, Match.Any);
        ES.update({
            index: ES.index,
            type: ES.type,
            analyzer: "polish",
            id: id,
            body: {
                doc: {
                    tags: document.tags,
                    author: document.author.profile.displayName,
                    content: document.content,
                    actors: document.actors
                }
            }
        }, ES.error);
    },
    EsDelete: function (id) {
        check(id, String);
        ES.delete({
            index: ES.index,
            type: ES.type,
            id: id
        }, ES.error);
    },
    EsBatchInsert: function (cursor) {
        check(cursor, Match.Any);
        ES.bulk({
            body: ES.createBatch(cursor)
        }, ES.error);
    },
    EsSuggest: function (text, field) {
        check(text, String);
        check(field, Match.Any);
        return ES.waitOnSuggest(text, field)
    },
    search: function (term, limit) {
        check(term, String);
        check(limit, Match.Any);
        var esResults = ES.waitOnSearch(term, limit);
        var ids = [];
        esResults.hits.hits.forEach(function (entry) {
            ids.push(entry._id);
        });
        return {
            hits: esResults.hits.total, result: Posts.find({
                '_id': {$in: ids}
            })
        };
    },
    EsSearch: function (word, size) {
        check(word, String);
        check(size, Number);
        return ES.waitOnSearch(word, size);
    }
});



